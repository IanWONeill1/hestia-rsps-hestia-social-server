# Hestia Login Server

Server that listeners for live game-servers or attempted lobby connections from players.
Collects and distributes world details.

## Dependencies

This repository requires hestia-server-core.jar in a libs folder to build.